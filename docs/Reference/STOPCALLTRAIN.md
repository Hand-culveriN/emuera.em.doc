---
hide:
  - toc
---

# STOPCALLTRAIN

| 関数名                                                                     | 引数 | 戻り値 |
| :------------------------------------------------------------------------- | :--- | :----- |
| ![](../assets/images/IconEmuera.webp)[`STOPCALLTRAIN`](./STOPCALLTRAIN.md) | なし | なし   |

!!! info "API"

    ```  { #language-erbapi }
	STOPCALLTRAIN
    ```
	[`CALLTRAIN`](./CALLTRAIN.md)命令が動いている間に呼び出されると、その時点でCALLTRAINの処理を終了します。  
	それ以外の場合は何もしません。  

!!! hint "ヒント"

    命令のみ対応しています。
