---
hide:
  - toc
---

# HTML_PRINT_ISLAND,HTML_PRINT_ISLAND_CLEAR

| 関数名                                                                                   | 引数     | 戻り値 |
| :--------------------------------------------------------------------------------------- | :------- | :----- |
| ![](../assets/images/Icondotnet.webp)[`HTML_PRINT_ISLAND`](./HTML_PRINT_ISLAND.md)       | `string` | なし   |
| ![](../assets/images/Icondotnet.webp)[`HTML_PRINT_ISLAND_CLEAR`](./HTML_PRINT_ISLAND.md) | `string` | なし   |

!!! info "API"

    ```  { #language-erbapi }
	HTML_PRINT_ISLAND htmlStyleString
    ```
	TAGを用いた記法は[`HTML_PRINT`](HTML_PRINT.md)と同じだが、行情報に依存しない。  
	行情報に依存しないため、通常の[`PRINT`](PRINT.md)命令と違っていくらスクロールしても消えることがない  
	詳細は[`HTML_PRINT`関連](../Emuera/HTML_PRINT.md)を参照してください。  

!!! hint "ヒント"

    命令のみ対応しています。
