---
hide:
  - toc
---

# COPYCHARA

| 関数名                                                             | 引数         | 戻り値 |
| :----------------------------------------------------------------- | :----------- | :----- |
| ![](../assets/images/IconEmuera.webp)[`COPYCHARA`](./COPYCHARA.md) | `int`, `int` | なし   |

!!! info "API"

    ```  { #language-erbapi }
	COPYCHARA charaID, charaID
    ```
	第一引数で指定された登録番号のキャラの全てのデータを第二引数で指定された登録番号のキャラにコピーします。

!!! hint "ヒント"

    命令のみ対応しています。
