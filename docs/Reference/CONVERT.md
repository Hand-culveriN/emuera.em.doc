---
hide:
  - toc
---

# CONVERT

| 関数名                                                         | 引数         | 戻り値   |
| :------------------------------------------------------------- | :----------- | :------- |
| ![](../assets/images/IconEmuera.webp)[`CONVERT`](./CONVERT.md) | `int`, `int` | `string` |

!!! info "API"

    ```  { #language-erbapi }
	
	string CONVERT value, ※
    ```
	※には2,8,10,16のみが入ります。  
	第一引数を2,8,10,または16進数で表現した文字列を返します。  

!!! hint "ヒント"

    命令、式中関数両方対応しています。

### 関連項目
- [変数の仕様と一覧>定数の表記](../Emuera/variables.md#_3)
