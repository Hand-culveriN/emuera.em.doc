---
hide:
  - toc
---

# RAND

| 関数名                                                   | 引数           | 戻り値 |
| :------------------------------------------------------- | :------------- | :----- |
| ![](../assets/images/IconEmuera.webp)[`RAND`](./RAND.md) | `int`(, `int`) | `int`  |

!!! info "API"

    ```  { #language-erbapi }
	int RAND min(, max)
    ```
	同名の変数とほぼ同じです。  
	`RAND(X)`は`RAND:X`と全く同じ動作をします。  
	乱数生成器は全く同じで、[`RANDOMIZE`](./RANDOMIZE.md)や[`INITRAND`](./RANDOMIZE.md)によって乱数を制御できます。  
	`RAND`関数では2つの引数を指定することができ、2つ指定された場合は1つ目の引数が乱数の最小値として使われます。  
	この関数は`0～18446744073709551615`(2の64乗-1)の間で生成した乱数を`max-min`で割った余りに`min`を加えて返します。  
	したがって、`max`は`min`より大きい（同じではだめ）値である必要があります。  
	`max-min`が符号付64ビット整数(`9223372036854775807``)の最大値を超える場合にエラーになります。  
	また、`max-min`が非常に大きい場合（1京くらい？）、余りを求める方法による偏りが無視できなくなります。  

!!! hint "ヒント"

    命令、式中関数両方対応しています。
