---
hide:
  - toc
---

# GCREATED

| 関数名                                                           | 引数  | 戻り値 |
| :--------------------------------------------------------------- | :---- | :----- |
| ![](../assets/images/IconEmuera.webp)[`GCREATED`](./GCREATED.md) | `int` | `int`  |

!!! info "API"

    ```  { #language-erbapi }
	int GCREATED gID
    ```
	指定した`gID`の`Graphics`が作成済みであるなら1を、未作成（廃棄済を含む）なら0を取得します。

!!! hint "ヒント"

    命令、式中関数両方対応しています。

### 関連項目
- [GCREATE](GCREATE.md)
