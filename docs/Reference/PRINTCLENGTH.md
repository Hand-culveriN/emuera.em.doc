---
hide:
  - toc
---

# PRINTCLENGTH

| 関数名                                                                   | 引数 | 戻り値 |
| :----------------------------------------------------------------------- | :--- | :----- |
| ![](../assets/images/IconEmuera.webp)[`PRINTCLENGTH`](./PRINTCLENGTH.md) | なし | `int`  |

!!! info "API"

    ```  { #language-erbapi }
	int PRINTCLENGTH
    ```
	コンフィグの[`PRINTCの文字数`](../Emuera/config.md#printc_1)の値を返します。RESULT = GETCONFIG("PRINTCの文字数")と同義です。  

!!! hint "ヒント"

    命令、式中関数両方対応しています。

### 関連項目
- [PRINTCPERLINE](PRINTCPERLINE.md)
