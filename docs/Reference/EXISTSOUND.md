---
hide:
  - toc
---

# EXISTSOUND

| 関数名                                                           | 引数     | 戻り値 |
| :--------------------------------------------------------------- | :------- | :----- |
| ![](../assets/images/IconEE.webp)[`EXISTSOUND`](./EXISTSOUND.md) | `string` | `int`  |

!!! info "API"

	``` { #language-erbapi }
	EXISTSOUND MediaFile
	```

	`sound`フォルダ内に、引数で指定したファイルが存在するか確認する  
	存在すれば1が、存在しなければ0を返す

!!! hint "ヒント"

    命令、式中関数両方対応しています。
