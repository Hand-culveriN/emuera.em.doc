---
hide:
  - toc
---

# CLEARTEXTBOX

| 関数名                                                                   | 引数 | 戻り値 |
| :----------------------------------------------------------------------- | :--- | :----- |
| ![](../assets/images/IconEmuera.webp)[`CLEARTEXTBOX`](./CLEARTEXTBOX.md) | なし | なし   |

!!! info "API"

    ```  { #language-erbapi }
	CLEARTEXTBOX
    ```
	最下部の入力欄のテキストを全て消去します。

!!! hint "ヒント"

    命令のみ対応しています。
