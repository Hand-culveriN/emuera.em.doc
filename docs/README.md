---
hide:
  - navigation
  - toc
---
# ようこそ

## EmueraEM+EEとは
> EmueraEM+EEは、[Emuera私家版](https://ux.getuploader.com/ninnohito/)をベースに改造を施したEmueraです。

### Emuera私家版
!!! quote "引用"

    Emuera本家が対応停止状態となっているため、私家改造版はコードのベースとするためにバグ修正以外の変更を行わないものとします

## ライセンス
> 本ドキュメントは、[CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/deed.ja)の利用範囲でご利用ください。

[![CC BY-NC 4.0](https://i.creativecommons.org/l/by-nc/4.0/88x31.png)](https://creativecommons.org/licenses/by-nc/4.0/deed.ja)

## ダウンロード
[**最新版:Emuera.NET 1824+v21+EMv18+EEv53**](assets/files/Emuera.NET%201824+v22+EMv18+EEv53+Rikaichan.zip)

## 連絡先
エラー報告、要望、編集の相談などは下記Discordサーバーにてご連絡ください

[ドキュメント編集チャンネル](https://discord.com/channels/428432103042973706/1236190714954514452)

<iframe src="https://discord.com/widget?id=428432103042973706&theme=dark" width="350" height="500" allowtransparency="true" frameborder="0" sandbox="allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts"></iframe>
