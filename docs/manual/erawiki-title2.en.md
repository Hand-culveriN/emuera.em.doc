# Variant creation/Title practice

Original page:  
[era series discussion thread, Summary Wiki V3, Title practice](https://seesaawiki.jp/eraseries/d/%a5%bf%a5%a4%a5%c8%a5%eb%bc%c2%c1%a9%ca%d4)  

---

- [Tutorial](erawiki-tutorial.md)
- [Title preparation](erawiki-title.md)
- Title practice
- [ERB creation practice](erawiki-ERBmanual.md)

---  

## `SYSTEM_FLOW.ERB`  
さて、タイトルを作ろうという見出しにもかかわらず、  
散々書いてきたのはタイトルを作らない方法だった。  
うんざりしている人もいるだろう。  

etc1821を開いてみよう。  
`SYSTEM_FLOW.ERB`というファイルがある。開いてみよう。  

```
;[ライセンス]パブリックドメイン  
;本ファイルに係る著作権を放棄する。  
;本ファイルに係る著作人格権は行使しない。  
;2015/11/01 MinorShift(Emuera作者)  
```

と書かれている。  
ツールの提供や優しいライセンス、wikiの案内だけではなく導入のフォローまでしてくれている。  
ありがたい。MinorShift様の懐の深さを崇め倒しながら書く。  

下のほうにスクロールしていくと、  

```
;@SYSTEM_TITLEが定義されていると、標準のタイトル画面の代わりにSYSTEM_TITLEが呼ばれる。  
@SYSTEM_TITLE  
```

と書かれている。  

『`;`』というのは、『ここから行末まで』はコメントだから処理として読み込まないでね、  
と伝えるための印だ。  
<!--//（2021/05/12 スレでご指摘いただいて表現を修正。感謝）  -->
つまり『;@SYSTEM_TITLEが定義されていると～』の行は、  
改変しようとしている人に対して、処理内容を説明してくれるための文章だ。  

もちろん、コメントは自分が忘れないために書くものでもあるが  
etc 1821フォルダの中身は全体的に教えるために書いてくれていることが伝わってくる。  
ERBを解読する、という作業の半分は、  
こうして日本語で残されているコメントをありがたく追っていく作業だ。  

この説明は、  
ERBフォルダ内にある、ERBファイルのどれでもいい、どこかに  

```
@SYSTEM_TITLE  
```

という行があったら、  
これまでずっと見てきたタイトル画面は表示されなくなって、  
代わりにこの下に書いた処理が表示されるよ、という意味だ。  

## `SYSTEM_FLOW.ERB`
Well, despite the heading about making a title,
I've written so much about how not to make a title.
Some of you may be tired of that.

Let's open etc1821.
There is a file called `SYSTEM_FLOW.ERB`. Let's open it.

```
;[License]Public Domain
;I waive the copyright of this file.
;I will not exercise any moral rights to this file.
;2015/11/01 MinorShift (Emuera creator)
```

... is what it says.
Not only does it provide tools, a friendly license, and wiki guides, it also follows up on the introduction.
I'm grateful. I'm writing this while admiring MinorShift's generosity.

If you scroll down,

```
;If @SYSTEM_TITLE is defined, SYSTEM_TITLE will be called instead of the standard title screen.
@SYSTEM_TITLE
```

is what's written there.

"`;`" is a mark to tell you that "from here to the end of the line" is a comment, so please do not read it as processing.
<!--// (2021/05/12 I corrected the wording after someone pointed it out in the thread. Thank you) -->
In other words, the line ``;@SYSTEM_TITLE is defined...'' is a sentence to explain the processing to those who are trying to modify it.

Of course, comments are written so that you don't forget, but the contents of the "etc 1821" folder convey that they were written to teach you as a whole.
Half of the work of deciphering ERB is gratefully following the comments left in Japanese like this.

This explanation means that if any ERB file in the ERB folder contains a line

```
@SYSTEM_TITLE
```

,
the title screen you have seen up until now will no longer be displayed,
and instead the process written below will be displayed.

---  

## 新ファイル作成！  
では、erakanonフォルダに戻ろう。  
ERBフォルダ内に  
『TITLE.ERB』を作ってみよう。  

<!--エンコードの差異は認められるのでこの記述はいらないはず
このとき注意することは、もしエンコードを設定せず作業しているなら  
フォルダの中で右クリックして新規作成を選ぶのではなく、  
ERBフォルダ内にある何かのファイルを開き、名前を変更して保存後、  
中身を削除して使用し、保存は『何か入力してから』することだ。  

まったくなにも入力していない状態で保存しようとすると、  
エディタが気を利かせて『削除しますか？』と尋ねてきたり  
空のファイルを開くとエンコードがデフォルトに設定されたりするので  
何かは入力してから保存しよう。  

これはエンコードを合わせるために行う。  
<!--
//(2021/05/12 wiki編集スレでご指摘いただいて変更＆追記。感謝。  
//2021/05/17 デフォルトのエンコードリストはエンコードってなに？の項目へ移動整理）  
もちろん確認して合わせられる人は新規作成しても良い。  
-->

---

## エンコードってなに？  
バリアントによって、エンコードというものが違う可能性がある。  

人間にとって文字は文字だが、パソコンは１文字１文字に数字を割り当て、  
『この数値ってことは、この文字を表示すればいいんだろ？』と判断している。  

エンコードはその判断基準で、なんの文字にどの数字を割り当てるかだ。  
動画等のエンコードと区別するためか、文字コードと呼ばれていたりもする。  

昔は特に統一されていなかったのでエンコードにはいくつか種類がある。
違う割り当てで読み込んでもらおうとすると、パソコンが混乱して文字化けする。

|エディタ|デフォルトのエンコード|変更方法|  
|-:|:-|:-|
|Windowsのメモ帳|Win10 201903以降なら<br>BOMなしUTF-8<br>それ以外ならShift-JIS|変更不可能|  
|秀丸エディタ|Shift-JIS|その他(O)→上級者向け設定をチェック→ファイル~~→エンコード1→新規作成やASCⅡのとき→変更(D)|  
|サクラエディタ|BOMなしUTF-8|設定→タイプ別設定一覧→基本→設定変更→ウィンドウタブ~~→デフォルトの文字コード|  
|Visual Studio Code|BOMなしUTF-8|Setting(`Ctrl+,`) のfiles.autoGuessEncoding 項目に<br>チェックを入れる(true)で、オートエンコードが機能|  
| | |Setting(Ctrl+,) の files.encoding 項目で<br>デフォルトのエンコードを変更|  
| | |ウィンドウ下部の右側に表示されているUTF-8を選び<br>表示されたアクション一覧からエンコード付きで再度開く<br>推測で出る一番上のJapanease（Shift JIS）を選ぶと一時的に変更|  

昔メジャーだったのは『Shift_JIS』で、  
今のeraなら『BOM付きUTF-8』がメジャーになってきている。  

この理由はいくつかあり、まとめwikiで解説してくれているかたがいる。  

- [システム改造Q&A→その他→文字コードには出来るだけUTF-8を使ってほしい](erawiki-modification-QandA.md#utf-8)|  

erakanonは製作年を見た通り昔の作品なので『Shift_JIS』だ。  
エンコードを表示してくれるエディタなら、そう表示されていると思う。  
だいたいエンコードや改行については一番右下に表示される。  

エンコードが混在していると上手く読み込まれず文字化けしたりする。  
長く改変するなら、全部のファイルをBOM付きUTF-8にしてしまうのがいいと思う。  

ひとつひとつ保存しなおすのは大変だ。エンコードの変更ツールなどを使って一気にやりたい。  
今のところ特に変更していない人は、とりあえずShift_JISで作ってみよう。  

<details><summary>今のところ読み飛ばしていい補足</summary>  

他にUTF-8が関わること。EmueraのERHで『#DIM SAVEDATA』を使ってセーブ可能なキャラ型変数や多次元文字列変数を定義するには、<br>
config『セーブデータをバイナリ形式で保存する』の設定をYESにすることが必須。<br>
YESにするとセーブデータは自動的にUTF-8で保存される。<br>

</details>

---  

## `@SYSTEM_TITLE`  
作成した『TITLE.ERB』がShift-JISになっていることを確かめて、  

``` { #language-erb title="ERB" }  
PRINTL タイトル画面  
WAIT  
```

と入力してみよう  

起動すると、さっきタイトル画面が表示されたタイミングで、  
『タイトル画面』  
という文字が表示され、停止しる。  

---  

## emuera.log  
クリックするとエラーが出る。  

``` { #language-erb title="ERB" }  
関数の終端でエラーが発生しました。  
予期しないスクリプト終端です。  
※※※ログファイルを～emuera.logに出力しました  
```

この「emuera.log」というのが、eraをいじると長くお付き合いするパートナーだ。  
一旦ゲームを閉じよう。  
Emuera1824.exeのあるところに、emuera.logというファイルが生成されている。  
ここにはどこでどんなエラーが起きたかが保存される。  

どの程度警告を貰うかはconfigで設定できる。  
Emuera製作者のひとり、`妊）|дﾟ)の人`様が  
おすすめの開発者向け設定を公開してくださっている。ありがたい。  

- [eratohoまとめ V3→Emueraについての補足→開発者のためのEmuera講座](Emuera-etc.md#emuera_1)

今はこれらの設定を自動的に行ってくれるワンクリック開発者モードというものもある。  

---  

## 関数の区切り、境目ってどこ？  
SYSTEM_FLOW.ERBの続きを見ていこう。  

『@なんとか』＝関数は、  
１ファイル内に１つしかないこともあるが、  
２つ以上ある場合、次の『@なんとか』が始まる前までがひとまとまりだ。  

なので、@SYSTEM_TITLEの中身は以下になる。  
まるごとTITLE.ERBに貼り付けしてみよう。  

``` { #language-erb title="ERB" }  
@SYSTEM_TITLE  
#DIMS VERSIONNAME  
;このタイミングでグローバル変数を読んでおけば取りこぼしが無くなる。  
;GLOBALはRESETDATAやLOADDATAによって初期化・上書きされない。  
;必要に応じてコメントアウトを解除すること。  
;LOADGLOBAL  

;バージョン表記をVERSIONNAMEに作成。  
;1001なら1.001、1100なら1.10と表示される  
VERSIONNAME = {GAMEBASE_VERSION / 1000}.%TOSTR(GAMEBASE_VERSION % 1000 / 10,"00")%  
SIF GAMEBASE_VERSION % 10 != 0  
	VERSIONNAME += TOSTR(GAMEBASE_VERSION % 10)  

;タイトル表示。  
DRAWLINE   

ALIGNMENT CENTER  
PRINTFORML %GAMEBASE_TITLE%  
PRINTFORML %VERSIONNAME%  
PRINTFORML %GAMEBASE_AUTHOR%  
PRINTFORML (%GAMEBASE_YEAR%)  
PRINTL   
PRINTFORML %GAMEBASE_INFO%  
ALIGNMENT LEFT  

DRAWLINE   

;選択肢表示  
$TITLE_SELECT  
PRINTSL "[0] " + GETCONFIGS("システムメニュー0");  
PRINTSL "[1] " + GETCONFIGS("システムメニュー1");  

$TITLE_INPUT  
INPUT  
IF RESULT == 0  
	RESETDATA  
	;ADDDEFCHARAはeramakerの初期化処理を再現するために存在する専用の関数です  
	;他の場面ではADDCHARAを使用してください  
	ADDDEFCHARA  
	BEGINWORD '= "FIRST"  
	CALL MAIN_LOOP  
ELSEIF RESULT == 1  
	CALL LOADGAME_EX  
	GOTO TITLE_SELECT  
	;LOADGAME_EXでLOADを行わずに戻ってきた場合、もう一度選択しなおす。  
ELSE  
	REUSELASTLINE 無効な値です  
	GOTO TITLE_INPUT  
ENDIF  
```

---  

## DIMってなに？  
一行目の  

``` { #language-erb title="ERB" }  
#DIMS VERSIONNAME  
```

を見てみよう。  

`DIM`と一般に呼ばれるこれは『ユーザー定義の変数』と呼ばれている。  
ユーザーとは、Emueraのユーザーのことなので、Emueraでゲームを作る人達のことだ。  
（プレイヤーはエンドユーザー）  

つまり、我々が定義していい変数、自分で好きな名前をつけていい変数という意味だ。  

日本語も使える。ただし変数名に日本語を使うことについて、意見は分かれるようだ。  
日本語名を積極的に使って可読性をあげよう派。  
日本語名は日本語文字列と組み合わせるとき混じってわかりにくい派。  
GREPで検出するとき地の文を検索でひっかけてしまうから英語のキーワードと組み合わせる派等。  
個人の考えによる。  

DIMは『@なんとか』の次の行に書くという決まりがある。  
（式中関数の場合は`#FUNCTION`や`#FUNCTIONS`が先に挟まるとか、  
　広域で使えるようにするERHという専用ファイルもあるが）  

``` { #language-erb title="ERB" }  
#DIM 好きな名前  
```

なら数字を入れられる箱  

``` { #language-erb title="ERB" }  
#DIMS 好きな名前  
```

なら文字列を入れられる箱  
のようだ。  

（だいたい末尾に`S`がつくと文字列版という意味になる。  
　`#FUNCTION`,`#FUNCTIONS` とかも同じだ。  
　文字列を英訳すると`character`,`string`。  
　.NETや他言語で文字列関連の処理を、StringクラスとかStringオブジェクトとか呼ぶ。  
　`S`はその略）  

このように書いて変数を作ることを『変数を宣言する』と表現する。  

他にもさまざまなパターンがある。  

- [EmueraWiki→eramaker basic 開発者向け情報→Emueraで追加された拡張文法→ユーザー定義の変数](../Emuera/user_defined_variables.md)  

中身が何であるかわかりやすく、検索しやすいように名前をつけて可読性を上げよう。  
複数人が同じ変数を別の用途で使い、意図せず上書きしてしまうことを防止しよう。  
などの目的で、初めから用意されている「`LOCAL`」「`LOCALS`」「`ARG`」「`ARGS`」  
「`A`」「`B`」「`C`」などの一文字、二文字の変数の代わりとして使われることが多い。  

- [eraシリーズを語るスレ　まとめWiki　V3→システム改造Q&A→基礎知識→プライベート変数について(A～ZやLOCALではなく#DIMで)](erawiki-modification-QandA.md#azlocaldim)  

古い処理はわかりにくいから面倒くさい。誰か全部`DIM`に直して欲しい。  
よくわかる。  
が、まとめてくれる人に『なんで最新化しないの？』と問うのはやめよう。  
みんなで遊び散らかして帰った後の部屋を見て『なんで掃除しないの？』と部屋主に問えば、  
悪鬼羅刹と化すだろう。そういうこともある。  

ここでは  

``` { #language-erb title="ERB" }  
#DIMS VERSIONNAME  
```

となっているので、  
`VERSIONNAME`と名付けられた、文字列の入る箱を作っている。  

---  

## グローバル変数ってなに？  
次の行を見てみよう。  

``` { #language-erb title="ERB" }  
;このタイミングでグローバル変数を読んでおけば取りこぼしが無くなる。  
;GLOBALはRESETDATAやLOADDATAによって初期化・上書きされない。  
;必要に応じてコメントアウトを解除すること。  
;LOADGLOBAL  
```

と書かれている。  
この行は、グローバル変数を使っていなければ無視していい。  

グローバル変数とはゲーム全体で使う変数だ。  

ひとくちに全体、といってもイメージが湧きにくいと思う。  

例えば、ゲームで遊んで、セーブデータＡを保存する。  
また新しくはじめて、セーブデータＢを保存する。  
データＡとＢの中身は干渉しない。  
これが普通のセーブできる変数だ。  

しかし、ＡＢ両方の情報が同じ場所に保存されることもある。  
タイトル画面から閲覧できる回想モードや、コンフィグ設定の保存などだ。  

タイトル画面から閲覧できる回想モードはたいてい、  
セーブデータに関わらずイベントを見さえすれば回想を閲覧できるようになる。  
Ａで見たイベント、Ｂで見たイベント、どちらもフラグが立っている。  
コンフィグ設定も、Ａで保存した設定をＢで読み込んだりできる。  

こういう、セーブデータに縛られない  
ゲーム全体で使う変数のことをグローバル変数という。  

セーブできる場合はグローバルセーブデータと読んだりもする。  
グローバルセーブデータはセーブデータとは別に、  
global.savというファイルで保存される。  

|変数名|性質|
|:-|:-|
|GLOBAL|SAVEGLOBAL命令によりセーブでき、LOADGLOBAL命令によりロードできる。|  
|GLOBALS|GLOBALの文字列型版。|  
|#DIM GLOBAL SAVEDATA 好きな名前|DIM版|  

などがこれに該当する。  

もしグローバル変数を使っているなら、ここの  

``` { #language-erb title="ERB" }  
;LOADGLOBAL  
```

のコメントアウト『;』を外して  

``` { #language-erb title="ERB" }  
LOADGLOBAL  
```

にして、ここでグローバル変数を読み込んでおくとゲームに反映されるので  
必要に応じて使ってね、ということだ。  

---  

## バージョンを計算して文字列化  
次の行を見てみよう。  

``` { #language-erb title="ERB" }  
;バージョン表記をVERSIONNAMEに作成。  
;1001なら1.001、1100なら1.10と表示される  
VERSIONNAME = {GAMEBASE_VERSION / 1000}.%TOSTR(GAMEBASE_VERSION % 1000 / 10,"00")%  
SIF GAMEBASE_VERSION % 10 != 0  
	VERSIONNAME += TOSTR(GAMEBASE_VERSION % 10)  
```

パッと見て絶望、なんじゃこりゃ。いきなりハードルたっけえわ。と半笑いになる。  

実は、1000で割っているだけだ。era basicの都合で、計算結果に小数点を扱えない。  

- [eratohoまとめ V3→開発関連→ERB構文講座2→小数の乗算](eratohowiki-ERBmanual.md#_36)  

<!--//（2021/05/12 スレでご指摘いただいて表現の修正と追記。感謝）  -->
そこでバージョンを表示する際、小数っぽく見えるように  
数字を文字列として整えて表示する準備をしている。  

なので、ここはこの計算にお任せして見なかったことにしてもいい。  
理解したい場合、ひとつひとつ見ていこう。  

### VERSIONNAME  
<!--//（2021/05/12 スレでご指摘いただいて修正。感謝）  -->
これは上のほうで宣言されているユーザー定義の変数だ。  
『DIMS』で宣言されていたので、文字列を格納できる。  

右側であれこれ計算しているように見えるが、  
結果は文字として扱っていることがわかる。  

### GAMEBASE_VERSION  
前半にでまくったGamebase.csvに関わる情報がついに現れた。  
『GAMEBASE_VERSION』には、Gamebase.csvで設定した  
バージョンが格納されている。  

ここで思い出して欲しいのは、バージョンというのは  
通常『1.01』とか『0.001』とか小数点で表示されているのに、  
Gamebase.csvには『1001』のように四桁の数値で書いたことだ。  

バージョン情報らしく見せるには1000で割れば済む。  
しかし小数点以下の結果はとれない。  

なので、  
文字列『`.`』の前に、4桁のバージョンを1000で割った数字を置く。  
文字列『`.`』の後に、4桁のバージョンを1000で割った数字の余りを、更に10で割って、数字を2桁でゼロ埋めして文字列として置く。  
更に端数があれば、その後ろに文字列として置く。  

すべてをくっつけると、小数っぽく見える文字列になる。  
ということをしている。  

（１桁目がメジャーバージョン  
　小数点以下１～２桁目がマイナーバージョン  
　小数点以下３桁目が不具合修正  
　と分割するイメージだと思う）  

こんな大変なことをするくらいならいっそ初めから文字列でいいのでは？  
という気がしてくるだろうが、そうもいかない。  

バージョンは数字であって欲しい理由がある。  
慣れないとちょっとわかりにくいが、  
同じ数字の『1』でも、パソコンに文字列として扱うように頼めば  
計算はできない。  
文字は文字という情報でしかなく、数字だろうと平文だろうと  
パソコン側に判別する基準がないからだ（判別する関数を作れば別）  

数字と文字列の違いのひとつは、  
計算したり、数値の大小を比較したりできるかどうかだ。  

バージョン0.8以下は互換性がない、と指定したい場合に、  
0.7はだめ、0.6もだめ…と文字列でひとつひとつ指定したくはない。  

Gamebase.csvで『バージョン違い認める？』一か所を指定するだけで、  
その数字未満を一括不可にできたほうが楽だ。  
そのためには数字でなくてはならない。  

そんなわけで、一見複雑なことをやっているように見えるのは、  
表示を整えるため苦肉の策のようだ。  

### `{~~}`ってなに？  
と、聞けばなんとなくやっていることはわかっても、  
それを自力で読み解けなければ意味がないと思うかもしれない。  
知らない記号がいくつか出てきている。  

『`{~~}`』はFORM文字列とかFORM構文とか書式付文字列とか呼ばれている。  

`PRINTFORM`や`CALLFORM`などの命令で使うと変数や変数の計算を展開できる。  
と表現しても今はわかりにくいかもしれないが、  
ERBを開いたことがあれば、口上や地の文によく使われる下のような文でおなじみだろう。  

``` { #language-erb title="ERB" }  
PRINTFORM 好感度が{CFLAG:TARGET:好感度}になった。  
```

『`{~~}`』の用途は二つある。  
ここでは用途その１『変数の中身、あるいは計算式の結果をくれ』という意味で使われている。  
<!--//(2021/05/12 wiki編集スレでご指摘いただいて修正。感謝）  -->

``` { #language-erb title="ERB" }  
VERSIONNAME = {GAMEBASE_VERSION / 1000}～  
```

を見てみよう。  

`GAMEBASE_VERSION`を1000で割っている。1000の位を取り出している。  
それが`{}`という記号で挟まれていることによって、結果を表示できる状態になる。  

Gamebase.csvで指定されたバージョンが『1』なら、それが格納された`GAMEBASE_VERSION`も『1』だ。  
『`/`』は『÷』という意味なので『`1 / 1000 = 0.001`』だが、  
eraは結果から小数点以下の数字を切り捨てる。結果は『`0`』になる。  

`GAMEBASE_VERSION / 1000`を`{}`で挟み、結果『`0`』は数字だが、その後に続く式が文字列なので、  
加算することで数字も文字列として扱われ、文字列として`VERSIONAME`に代入される。  

『`{~~}`』の用途その２『行の連結』は、ここではまだ関係ないので、  
Wikiの説明されているページを貼っておく。  

- [EmueraWiki→eramaker basic 開発者向け情報→Emueraで追加された拡張文法→一般→行の連結](../Emuera/expression.md#_2)  

---  

### `%~~%`ってなに？  
『`%~~%`』は『文字列変数の中身、あるいは文字列を使った計算式の結果をくれ』という意味だ。  

数字ではなく文字列に対して使う。  
要するに『`{~~}`』用途その１『変数の中身、あるいは計算式の結果をくれ』の文字列版だ。  
<!--//(2021/05/12 wiki編集スレでご指摘いただいて修正。感謝）  -->

``` { #language-erb title="ERB" }  
VERSIONNAME = {GAMEBASE_VERSION / 1000}.%TOSTR(GAMEBASE_VERSION % 1000 / 10,"00")%  
```

と書かれている。`{GAMEBASE_VERSION / 1000}`の次にある『`.`』はただの文字列だ。  

``` { #language-erb title="ERB" }  
%TOSTR(GAMEBASE_VERSION % 1000 / 10,"00")%  
```

を見てみよう。『`%%`』に挟まれている。  
`GAMEBASE_VERSION`は数字なのに、なぜ文字列？　と思うかもしれない。  

``` { #language-erb title="ERB" }  
GAMEBASE_VERSION % 1000 / 10  
```

は、1000で割って余りを計算したり、それを更に10で割ったりしている。  
数字でなくてはできないことだ。  
そこに『`TOSTR()`』が関わってくる。  

---  

### `TOSTR()`ってなに？  
『文字列に変えてくれ』という意味だ。  

`TOSTR()`はEmueraに最初から用意されている、式中で使える関数、というものだ。  

- [EmueraWiki→eramaker basic 開発者向け情報→Emueraで追加された拡張文法→式中で使える関数→str TOSTR(int value, str format = "")](../Reference/TOSTR.md)  

式中で使える関数には色々便利な機能のものがあるが、大量すぎて覚えるのは難しい。  

デフォルトの変数名や`DIM`宣言された変数名ではなく、  
謎の英単語風のところはだいたい命令だが、命令には`()`がつかない。  
ついていたら関数だが、`@`や`CALL`のないところに唐突にある場合は式中で使える関数だ。  
この式中で使える関数の一覧がのっているページで検索してみよう。  
（この判断が正しいかはともあれ、だいたいこれで探せると思う）  
[命令・式中関数一覧](../Reference/README.md)

CTRLキーとFキーを同時押しすると、  
ブラウザのページ内検索ウィンドウが表示される。  

検索するとき、`()`の中身まで検索しようとすると見つからない。  
`()`より前の単語だけ抜き出して検索しよう。  

もしない場合、そのバリアントの内部でgrep検索してみよう。  
処理が書かれているところを探したいので『@検索したい単語』で検索する。  
見つけられたら、誰かが自作した式中で使える関数だったということだ。  

- [eraシリーズを語るスレ　まとめWiki　V3→システム改造Q&A→基礎知識→GREPのやり方](erawiki-modification-QandA.md#grep)  

（エディタにVisualStudio Code等を使っていると、関数名や式中関数名をクリックしたら  
　自動的にそれが記述されているファイルを開く機能を公開してくれてる人がいる）  

ともあれ、`TOSTR`は数字を文字列に変換する機能だとわかる。  

数字にカンマをつけたり、ゼロで埋めて桁数をそろえたいときに便利な機能だ。  

ここでは、  
`01、02、03…`という風に、数字が1桁でもゼロで埋めて2桁で表示して欲しい  
という意味で、"`00`"と指定している。  
（こうやってゼロで埋めることをゼロパディングと読んだりする）  

---  

### `SIF`ってなに？  

``` { #language-erb title="ERB" }  
SIF GAMEBASE_VERSION % 10 != 0  
	VERSIONNAME += TOSTR(GAMEBASE_VERSION % 10)  
```

を見てみよう。  

`SIF`は条件分岐というやつだ。  
`IF～ELSE～ENDIF`のような条件分岐の簡略化で、一行で行う`IF`文だ。  

- [eramaker ERBファイル書式（暫定版）→変数と命令→命令について→条件判断する](../eramaker/ERB_format.md#_4)  

`SIF`は条件式が0でなければ（成立した場合）次の行を実行します。0の場合（成立しない場合）、次の行をスキップします。  
と書かれている。  

``` { #language-erb title="ERB" }  
SIF XXXXX  
```

の、`XXXXX`のところが条件を書くところだ。  
その次行が、条件が通ったときに実行する処理だ。  

``` { #language-erb title="ERB" }  
SIF GAMEBASE_VERSION % 10 != 0  
```

の場合  
`GAMEBASE_VERSION`を10で割った余り（%で余りを計算）が、  
0でなければ（`!`は否定という意味。`!=`だとイコールの否定、要するに同じではないという意味になる）  
という意味になる。  

10で割ってゼロにならないということは、1の位があるということだ。  
余りは1の位の数そのものになる。  

なので、  

``` { #language-erb title="ERB" }  
SIF GAMEBASE_VERSION % 10 != 0	;1の位があれば  
	VERSIONNAME += TOSTR(GAMEBASE_VERSION % 10)	;1の位を文字列として追加してくれ  
```

という意味になる。  

※`SIF`は処理が増えて行数が増えると`IF`文に書きなおさなくてはならなかったり、  
　コメントをつけると行がわかりにくくなったり  
　処理をコメントアウトすると次の行に影響したりすることから、あまり好まない人もいる。  
　一方で、慣れると作っているときは便利なので多用してしまいがちでもある。個々の考えによる。  

---  

### 補足 `=`と`'=`  

``` { #language-erb title="ERB" }  
VARSIONNAME =   
```

は、今なら  

``` { #language-erb title="ERB" }  
VARSIONNAME '= ""  
```

<!--//(2021/05/12 スレでご指摘いただいて修正。感謝)  -->
と書くこともできる。  

- [EmueraWiki→Emueraで追加された拡張文法→一般→文字列式を用いた文字列変数への代入](../Emuera/expression.md#_9)

これは元は使えなかった書き方で、Emueraが進化して使えるようになった。  
文字列の代入である、ということがわかりやすい。  

ただしこれをすると『`文字列変数名 '= "あああ"`』のように`"~~"`で囲う必要がある。  

ちょっと面倒にもなるが、日本語の変数と文字列の日本語が入り乱れているときでも混乱しにくい。  
また、末尾に空白が入った文字列や空白のみの文字列を代入する場合にパッと見てわかりやすい。  
<!--//(2021/05/12 スレでご指摘いただいて追記。感謝)  -->

---  

## メイン部分の表示  
ようやく次の処理を見てみよう。  

``` { #language-erb title="ERB" }  
;タイトル表示。  
DRAWLINE   

ALIGNMENT CENTER  
PRINTFORML %GAMEBASE_TITLE%  
PRINTFORML %VERSIONNAME%  
PRINTFORML %GAMEBASE_AUTHOR%  
PRINTFORML (%GAMEBASE_YEAR%)  
PRINTL   
PRINTFORML %GAMEBASE_INFO%  
ALIGNMENT LEFT  

DRAWLINE   
```

とある。  

---  

### `DRAWLINE`って？  
区切り線を引いて欲しいという意味だ。  
シーン変更、見出し作りなどでよく使う。  

- [リファレンス→`DRAWLINE`](../Reference/DRAWLINE.md)

>DRAWLINEを使うと画面の左端から右端まで----と線を引きます。  

と書かれている。  

---  

### `_Replace.csv`  
`DRAWLINE`は、Emueraのデフォルト設定では『`-`』を繋げたような線になっている。  
これを隙間のない線『`─`』にしたり、  
あるいは『`=`』にして二重線にしたい人もいると思う。  
`DRAWLINE`を使ったとき表示される線を変えるにはどうしたらいいのか。  

- [EmueraWiki→eramaker basic 開発者向け情報→_replace.csv](../Emuera/replace.md)

etc1821フォルダ内に_Replace.csvというファイルがある。  
これをコピーしてerakanonフォルダ内CSVフォルダ内にペーストしてみよう。  

``` { #language-erb title="ERB" }  
;DRAWLINEの表示文字  
;DRAWLINEで表示する文字  
;DRAWLINE文字 , (半角文字)  
;DRAWLINE文字 , +  
```

と書かれている行がある。  

``` { #language-erb title="ERB" }  
;DRAWLINE文字 , +  
```

から  
『`;`』を消して  

``` { #language-erb title="ERB" }  
DRAWLINE文字 , +  
```

にして保存してみよう。  

Emueraを起動すると、それまで  

```
------  
```

という線だったところが  

```
++++++  
```

という線になったはずだ。  
これだけでも若干タイトル画面にオリジナリティが出た気がしてくる。  

線の種類を使い分けたい人もいるだろう。  
大見出しと小見出しで線の種類を分けたい、日付変更時だけ太い線を引きたいなど。  

``` { #language-erb title="ERB" }  
CUSTOMDRAWLINE <文字列>  
```

という命令、  

``` { #language-erb title="ERB" }  
DRAWLINEFORM <FORM文字列>  
```

という命令方法がある。  

- [リファレンス→`CUSTOMDRAWLINE`、`DRAWLINEFORM`](../Reference/CUSTOMDRAWLINE.md)

``` { #language-erb title="ERB" }  
CUSTOMDRAWLINE ─  
```

のように、その都度線に使いたい記号を指定して書く。  

---  

### `ALIGNMENT CENTER`って？  

- [リファレンス→`ALIGNMENT`](../Reference/ALIGNMENT.md)

`ALIGNMENT` アラインメントとは、並べるとか整列とかいう意味だ。  
文字を左揃えにしたり、中央揃えにしたり、右揃えにしたりする指定になっている。  

``` { #language-erb title="ERB" }  
ALIGNMENT RIGHT ;右揃え  
ALIGNMENT CENTER ;中央揃え  
ALIGNMENT LEFT ;左揃え  
```

通常は左揃えだが、ここではタイトル画面らしく中央に揃えている。  

---  

### `PRINTFORML`って？  
文字列を表示する命令の一種だ。  

- [リファレンス→`PRINT`](../Reference/PRINT.md)

いきなり、`PRINT(|V|S|FORM|FORMS)(|K|D)(|L|W)`  
という謎の記述を見て、頭を抱えた人もいるかもしれない。  

`PRINT`自体は、文字を表示する命令だ。  
`PRINT`の後、半角スペースをひとつ空け、文字を書き込もう。  

``` { #language-erb title="ERB" }  
PRINT ああああ  
```

のように。  

そして`PRINT`の後に続く複雑ななにか。気になると思う。  
`PRINT`のあと、カッコが3つに別れている。  
いくつかのアルファベットや単語が『`|`』で区切られている。  

これは、欲しい機能を`PRINT`にくっつけて使えるよ。という意味だ。  

この『`|`』で区切られたアルファベットや単語には、それぞれ別の機能が割り当てられている。  
１カッコ内の機能はひとつしか選べないが、別のカッコの機能とは組み合わせて使える。  
『`(|`』になっているところは、『(なし|』という意味なので、省略できる。  
<!--//（2021/05/12 スレにご指摘いただいて変更。感謝）  -->

このように、みっつのカッコから一つだけ選んで使うこともできるし  
`PRINTV / PRINTS / PRINTFORM / PRINTFORMS / PRINTK / PRINTD / PRINTL / PRINTW`  

このように、ひとつめ、ふたつめのカッコからひとつ、選んでくっつけることもでき  
`PRINTVK / PRINTSK / PRINTFORMK / PRINTFORMSK / PRINTVD / PRINTSD`  

このように、ひとつめ、ふたつめ、みっつめのカッコからひとつずつ選んでくっつけることもできる  
`PRINTVKL / PRINTSKL / PRINTFORMKL / PRINTFORMSKL / PRINTVDL / PRINTSDL`  

ひとつめのカッコからひとつ、みっつめのカッコからひとつなどもだ。  

eraは口上や地の文に、文字列変数化されたあなたやキャラの名前を多用することが多い。  
そのため`{~~}`や`%~~%`を使える`PRINTFORM`系はよく使われる。  

また、配置を整える`PRINTC`系。  
`PRINTBUTTON`系で選択肢を表示して`INPUT`で受け取る。  
ランダムテキスト表示に`PRINTDATA`系を使う。  

などなど、`PRINT`関連は便利で基礎的な情報だ。  

ここでは『`PRINTFORML `』なので、  
『`PRINT`』文字を表示するよ。という命令に加えて、  
『`FORM`』書式付文字を使うよ。  
『`L`』改行してねクリックはいらないよ。  
という機能を組み合わせていることになる。  

---  

### 各表示  
前半、Gamebase.csvについて話したが、その情報が格納されている変数が羅列されている。  

中身はここに書かれている通りだ。  

- [EmueraWiki→eramaker basic 開発者向け情報→Emueraで追加された拡張文法→定数・変数](../Emuera/variables.md#gamebasecsv)  

こうしてタイトル画面でGamebase.csvの情報を利用することにより、  
Gamebase.csvだけ修正すれば、タイトル画面を弄らなくても更新できることになる。  

``` { #language-erb title="ERB" }  
DRAWLINE   

ALIGNMENT CENTER  
PRINTFORML %GAMEBASE_TITLE%  
PRINTFORML %VERSIONNAME%  
PRINTFORML %GAMEBASE_AUTHOR%  
PRINTFORML (%GAMEBASE_YEAR%)  
PRINTL   
PRINTFORML %GAMEBASE_INFO%  
ALIGNMENT LEFT  

DRAWLINE   
```

Gamebase.csvで整えた情報と、  
バージョンの数字を文字列化して整えた`VERSIONNAME`、  
空行が欲しいところに`PRINTL`を使い、  
中央寄せを左寄せに直し、  
もう一度区切り線を引いて、  

タイトルの表示は終了だ。  

---  

### 独自のデザインに  
文字を左寄せの表示形式に変えたり、欲しい情報を足したり別のところで表示したい情報を消したり、  
GAMEBASE_TITLEを省いてアスキーアートに変えたり、画像を表示したり、お好みで改変するといいだろう。  

フォントの表示、色変え、特殊な表示など  

- [リファレンス→`PRINT系`](../Reference/PRINT.md)
- [リファレンス→表示操作・フォント操作・表示仕様参照](../Reference/README.md#_3)
- [リファレンス→`HTML_PRINT`](../Reference/HTML_PRINT.md)

画像の表示  

- [リファレンス→`PRINT_IMG`](../Reference/PRINT_IMG.md)
- [リソースファイルについて](../Emuera/resources.md)
- [リファレンス→`HTML_PRINT`](../Reference/HTML_PRINT.md)
- [リファレンス→画像処理関連](../Reference/README.md#_14)

また、`WINDOW_TITLE`は代入可能。  
左上のウィンドウタイトルに何か表示を加えたりしても良いかもしれない。  
`少女祈祷中...`のローディング表示を_Replace.csvで変更したりもできる。  

---  

## 選択肢  
見た目は弄れるようになってきたと思うが、  
ボタンを表示して選んでもらう、ということができないとゲームにはしにくい。  

続きを見ていこう。  
タイトル画面だからか、_Replace.csvを利用したやや特殊なボタンになっている。  

``` { #language-erb title="ERB" }  
;選択肢表示  
$TITLE_SELECT  
PRINTSL "[0] " + GETCONFIGS("システムメニュー0");  
PRINTSL "[1] " + GETCONFIGS("システムメニュー1");  
```

---  

### `$TITLE_SELECT`  

ここまでの説明だと

``` { #language-erb title="ERB" }  
$TITLE_SELECT  
```

というのが見慣れない行だ。  

処理は基本的に上から下へ流れるのだが、  
行ったり来たりさせたり、同じところをグルグル回らせることもできる。  

『$好きなラベル名』と書くと、そこに『ラベル』を貼ることができる。  
本の重要なところに貼っておく付箋や、チャットツールのピンようなものだ。  
『`GOTO 好きなラベル名`』と同じ関数内の他の場所に書くと、指名したラベルのところへ戻れる。  

ここはロード画面からロードせず戻ってきたとき、  
もう一度ボタンを表示するための戻り先として貼られているラベルのようだ。  

- [システム改造Q&A→基礎知識→関数の中での移動方法（制御構文）](erawiki-modification-QandA.md#_2)

`GOTO`文は可読性を損ないやすいため、入れ子ループから一気に抜け出す場合以外は、  
できるだけ使わないほうが良いそうだ。`LOOP`文か`WHILE`文で置き換えることを推奨されている。  
少し難しいかもしれないが理解はできなくてもいいので  
『スパゲティプログラム』あるいは『スパゲティコード』などで検索してみよう。  
<!--//（2021/05/12 リンクを貼っていいか迷って検索案内だけ書いてみました。ご指摘感謝）  -->

ということなので、繰り返し処理の書き方も教わろう。  

- [リファレンス→ループ・分岐構文](../Reference/README.md#_10)
- [システム改造Q&A→基礎知識→繰り返し処理の書き方(FORとREPEATの違いと推奨書式)](erawiki-modification-QandA.md#forrepeat)

よくわからない場合は、わかるまでとりあえずそのままお借りしよう。  

---  

### ボタン  

``` { #language-erb title="ERB" }  
PRINTSL "[0] " + GETCONFIGS("システムメニュー0");  
PRINTSL "[1] " + GETCONFIGS("システムメニュー1");  
```

『`GETCONFIGS()`』を使用した少々変則的なボタンだ。  

`PRINTSL`は、  
文字を表示してくれる『`PRINT`』  
文字列式を表示してくれる『`S`』  
クリックなしで改行してくれる『`L`』  
を組み合わせた命令だ。  

文字を`"~~"`で囲み、文字列式に足して表示している。  

そして『`GETCONFIGS()`』という式中で使える関数を使って、  
_Replace.csvのデータを呼び出している。  
『`GETCONFIGS()`』は『replace.csvの設定項目を整数または文字列で取得』する。  

_Replace.csvを開いてみると、  

``` { #language-erb title="ERB" }  
;タイトルでのシステムメニュー表示1  
;起動画面での『[0] 最初からはじめる』の文字列部分  
;システムメニュー0 , (文字列)  
システムメニュー0 , 最初から調教  
;タイトルでのシステムメニュー表示2  
;起動画面での『[1] ロードしてはじめる』の文字列部分  
;システムメニュー1 , (文字列)  
システムメニュー1 , 調教の続きを行う  
```

と書かれている。  

;が行頭についているところはコメントなので、読み込みされない説明文だ。つまり  

``` { #language-erb title="ERB" }  
システムメニュー0 , 最初から調教  
システムメニュー1 , 調教の続きを行う  
```

を置き換えれば、  
『最初から調教』を『Game Start』に変えたりできるということだ。  

`[0]`や`[1]`についてはEmueraWikiの`PRINTBUTTON`命令に書かれていることを読むと  
ボタン表示について全体的に書かれているので把握しやすいかもしれない。  

- [リファレンス→`PRINTBUTTON`](../Reference/PRINTBUTTON.md)

---  

## 選択結果  
次を見てみよう。  

``` { #language-erb title="ERB" }  
$TITLE_INPUT  
INPUT  
IF RESULT == 0  
	RESETDATA  
	;ADDDEFCHARAはeramakerの初期化処理を再現するために存在する専用の関数です  
	;他の場面ではADDCHARAを使用してください  
	ADDDEFCHARA  
	;BEGINWORD '= "FIRST"  
	;CALL MAIN_LOOP  
	BEGIN FIRST  
ELSEIF RESULT == 1  
	LOADGAME  
	GOTO TITLE_SELECT  
	;LOADGAME_EXでLOADを行わずに戻ってきた場合、もう一度選択しなおす。  
ELSE  
	REUSELASTLINE 無効な値です  
	GOTO TITLE_INPUT  
ENDIF  
```

---  

### `INPUT`  

``` { #language-erb title="ERB" }  
INPUT  
```

は、入力を待て、という命令だ。  

ボタンを出したら必ずどこかでこれを出す。  
でないとゲームが勝手に進んでいくのでプレイヤーはボタンを選べない。  

手で入力して0と入力した場合、  
入力値として0の指定されたボタンをマウスでクリックした場合、  
どちらも同じように0を入力したとみなされる。  

なので、マウスでボタンをクリックされる、  
あるいは、番号を手入力されてエンターキーを押される、  
どちらかの操作をされるまで、待機せよという命令になる。  

文字列を受け取りたい場合は  

``` { #language-erb title="ERB" }  
INPUTS  
```

という命令もある。  

``` { #language-erb title="ERB" }  
INPUT 0  
```

と書くと、  
何も入力せずエンターキーをクリックしたとき、0が入力される。  

『エンターキー押しっぱなしにしたら大量の選択肢に全て0を返して流す』  
といった操作が可能になり、テストプレイ時に便利だ。  

『`$TITLE_INPUT`』は『`$TITLE_SELECT`』と同じラベルだ。  
無効な値だったとき、もう一度入力待ちするために呼ばれるので  
`INPUT`の上の行に置かれている。  

---  

### IF  
これは『IF文』とか『条件式』とか『分岐』とか『条件分岐』とか呼ばれるものだ。  
「もし～なら、～しろ」と命令をする。  
ゲームの大半は条件分岐によって作られていると言っても過言ではない。  

もし好感度が1000をこえたら恋慕をつける。  
もし所持金が1億円をこえたらゲームをクリアする。  
などなど。  

オープニングで目標を提示する。  
プレイヤーがボタンを操作したら、結果としてステータス値を変更する。  
条件を達成したらエンディングを表示する。  

という流れがゲームの基本処理だ。  

使い方その１  

``` { #language-erb title="ERB" }  
IF 条件  
	条件が成立していたときの内容  
ENDIF  
```

使い方その２  

``` { #language-erb title="ERB" }  
IF 条件  
	条件が成立していたときの内容  
ELSE  
	条件が成立しなかったときの内容  
ENDIF  
```

使い方その３  

``` { #language-erb title="ERB" }  
IF 条件その１  
	条件その１が成立していたときの内容  
ELSEIF 条件その２  
	条件その２が成立していたときの内容  
ELSE  
	条件その１もその２も成立しなかったときの内容  
ENDIF  
```

`ELSEIF`はいくつでも指定できる。  

ちなみに『`IF 条件`』の次行に、行頭空欄がある。  
ここはタブキーを押している。  

`IF`文はマトリョーシカのように、`IF`文の中に更に`IF`文が入ることがある。  
入れ子構造とか呼ぶのだが、もしこれが全て行頭から始まっていたら大変わかりにくい。  

そこで`IF`文の中に書く処理は、必ずタブキーを一回押して行頭を下げる。  
１タブ下げたところにまたIF文を書いたら、その処理はまた１タブ行頭を下げる。  
といったことをして、入れ子構造をわかりやすくする。  

（入れ子はいくら形を整えても複雑なので、避けられるなら避けたほうがいい。  
　また、完全な新バリアントを作るなら半角スペースの利用も考えられる。  
　大手最新のコーディングルールではタブを使わず半角スペースを使用するよう指定されていたりして、この場合、スペースの数まで用途に応じて具体的に決まっていたりする。  
　しかし、整合性がとれていないことが一番わかりにくい。  
　そのためタブが使用されている既存作品をお借りするときはタブを使ったほうが良い）  

行頭の位置を周りの文章よりも下げることを『字下げ』とか『インデント』と呼ぶ。  

入れ子の`IF`文が大量かつインデントを揃えていないパッチはときどき、製作仲間を発狂させる。  
`IF`文を書くときは注意しよう。  

見たことのある人もいるかもしれないが、eraの場合は  
『数百個あるコマンドの数だけ条件分岐がある』になりかねない。  
これが途中でどこか一つでもずれると、続きが全部ずれてしまうのだ。  

また『ひとつの変数に対し、中身の数字が1のとき、2のとき、3のとき……』  
のような条件分岐を行いたい場合は『`SELECTCASE`』文で条件式を略すことがおすすめされる。  
こちらで紹介してくれている。  

[システム改造Q&A→基礎知識→`IF・ELSEIF`のかたまりは`SELECTCASE`文に出来るかも](erawiki-modification-QandA.md#ifelseselectcase)

ちょうど今見ている`INPUT`の`RESULT`分岐などは`SELECTCASE`文にしやすいところだ。  
試してみてもいいかもしれない。  

---  

### `RESULT == 0`  
条件が『`RESULT == 0`』になっている。  
唐突に出てきた『`RESULT`』は、最初から用意されている変数だ。  
『`INPUT`』でプレイヤーによって選択されたボタンや手入力の入力値は  
この『`RESULT`』に自動的に保存される。  
（`INPUT`に限らず、関数の『`RETURN なんとか`』を受け取ったりもする）  

この『`RESULT`』は非常によく使われるので中身が入れ替わりやすい。  
なので受け取ったらすぐ、自前で作った`DIM`変数に保存して  
そちらを使う癖をつけたほうが良いらしい。  

複雑化してる最近のeraでは`RETURN`系は`RESULT`がすぐ迷子になるので  
`RESULT`で受け取って保存して使うのは不具合の原因にもなりうる。  
これを`#DIM REF`で定義できる参照型変数で解決できる場面もある。  
本来はプライベート変数(`LOCAL`変数)は複数の関数では共有できないが、  
この参照型変数を引数に指定すれば関数内での代入等が呼び出し元の関数に反映される  

できるだけ式中関数を使えるときはそちらで、というアドバイスもこちらに紹介されている。  

- [システム改造Q&A→基礎知識→`RESULT`と式中関数](erawiki-modification-QandA.md#result)

EmueraWikiの「式中で使える関数」の項には  
`RESULT`や`RESULTS`への代入は行われません  
と書かれているが、例外がある。  
「`CHKDATA()`」「`CHKCHARADATA()`」「`FIND_CHARADATA()`」がこれにあたる。  
また、式中関数内なら絶対に`RESULT`が書き換えられないわけでもなく、`RESULT`変数を使用する命令を使えば普通に書き換えられてしまう。  
`CALL`も使ってない、代入したわけでもない、数行のコードなのに何故かうまく動かない、そんな時は`RESULT`の誤爆を疑ってみよう。  

ここでは`INPUT`直後の分岐に使われていて、  
上書きされる心配はなさそうなのでそのまま進める。  

『`RESULT == 0`』は『INPUTの入力結果が0だったなら』  
ということなので、  
『最初から調教する』  
が選ばれたら、という意味になる。  

---  

### `RESETDATA`  
『最初から調教する』が選ばれた場合に行う処理を見ていこう。  

``` { #language-erb title="ERB" }  
RESETDATA  
```

そのまま、データをリセットして欲しいという命令だ。  

- [リファレンス→`RESETDATA`](../Reference/RESETDATA.md)

これを命令しておかないと、すでにゲームで遊んでから  
『タイトルに戻る』で戻ってきた場合に、  
他のデータが残ってしまったりする。  

---  

### `ADDDEFCHARA`  

``` { #language-erb title="ERB" }  
;ADDDEFCHARAはeramakerの初期化処理を再現するために存在する専用の関数です  
;他の場面ではADDCHARAを使用してください  
```

と書かれている。  

- [リファレンス→`ADDDEFCHARA`](../Reference/ADDDEFCHARA.md)

eramakerとの互換性を保つための命令だ。CSVの存在するキャラを一気に登録する。  

CSVにキャラクターデータを追加しても、  
こうして読み込まなければキャラは追加されない。  

バリアントによっては初めから`ADDCHARA`で設定している場合もある。  
空のキャラを作る`ADDVOIDCHARA`を用意し、後から設定を足してカスタムキャラにする場合もある。  

CSV番号は`1,3,7`等とばして設定することもできるが、  
キャラが登録されるときは詰めて登録される。  

`CFLAG`とか`BASE`といった、初めから用意されているキャラ用のデータを扱うとき  
指定するキャラ番号は、CSV番号ではなく、登録された順番のほうになる。  
（CSV番号のほうはEmueraWikiなどでも「`NO`（Numberの意）」と呼ばれることもある。後者は「登録番号」と呼ばれることもある）  

---  

### `BEGINWORD '= "FIRST"`  
次の行を見てみよう。  

``` { #language-erb title="ERB" }  
BEGINWORD '= "FIRST"  
CALL MAIN_LOOP  
```

今見ている処理は、SYSTEM_FLOW.ERBから抜き出してきたものだ。  
SYSTEM_FLOW.ERBは、Emueraでの一連の流れを案内してくれるファイルだ。  

『`BEGINWORD`』は、処理に必要で作った変数というよりは、  
案内しやすくするために作ってくれた変数である。  
『`BEGINWORD`』が宣言されている場所は、『SYSTEM_FLOW.ERH』だ。  

拡張子が『.ERH』になっているファイルは、  
関数内だけではなくあちこちの関数で使いたい`DIM`の宣言を書くファイルだ。  

etc1821フォルダから、SYSTEM_FLOW.ERHをコピーし、  
erakanonフォルダのERBフォルダにペーストすれば動くようになるが、  
ここでは行わない。  

今回はフローをたどろうという話ではなく、バニラ環境を作ろうという趣旨なので  
`BEGIN FIRST`に書き換えてみよう。  

``` { #language-erb title="ERB" }  
BEGINWORD '= "FIRST"  
CALL MAIN_LOOP  
```

を  

``` { #language-erb title="ERB" }  
BEGIN FIRST  
```

にしてみよう。  

ゲームを起動して、『`[0]最初から調教`』を選んだら、  
`@SYSTEM_TITLE`をつけ足してから起きていたエラーが消えて  
ゲームが始まるようになったはずだ。  

---  

### `ELSEIF RESULT == 1`  
次の行を見てみよう。  

``` { #language-erb title="ERB" }  
ELSEIF RESULT == 1  
	CALL LOADGAME_EX  
	GOTO TITLE_SELECT  
	;LOADGAME_EXでLOADを行わずに戻ってきた場合、もう一度選択しなおす。  
```

`ELSEIF RESULT == 1`は、  
`[1] 調教の続き`を行う  
が選ばれた場合、という意味になる。  

`CALL XXXX`というのは、関数（@なんとか）を呼び出す命令だ。  
『`@LOADGAME_EX`』という関数を呼び出して欲しいという意味になる。  

こちらも『SYSTEM_FLOW.ERB』内にある関数の呼び出しなので、  
『`@LOADGAME_EX`』をお借りしたい場合はコピーしてこよう。  

Emuera本家のetcフォルダ以外に、eraバリアントには  
CCライセンスで解放してくれている関数がたくさんあり、  
それらをライセンスごとコピーしてお借りすることで  
使わせてもらえることが多い。  
１から作る場合でもありがたくお借りできると思う。  

バニラデフォルトのシステムをお借りしたい場合は、  

``` { #language-erb title="ERB" }  
ELSEIF RESULT == 1  
	LOADGAME  
	GOTO TITLE_SELECT  
```

と書きかえれば動くようになる。  

『`GOTO TITLE_SELECT`』は『`$TITLE_SELECT`』の項でも書いたが  
ロードせずタイトル画面に戻ってきたとき、再表示するために戻る命令だ。  

---  

### `ELSE`  

``` { #language-erb title="ERB" }  
ELSE  
	REUSELASTLINE 無効な値です  
	GOTO TITLE_INPUT  
ENDIF  
```

`ELSE`はそれ以外、という意味になる。  
ここでは案内のためにだろうか『`REUSELASTLINE `』という、  
最終行を指定した書式付き文字列で書き換える命令で  
無効な値であることを表示してくれている。  

- [リファレンス-`REUSELASTLINE`](../Reference/REUSELASTLINE.md)

手入力を無視したい場合は問答無用で戻してしまってもかまわないと思う。  

---  

### `ENDIF`  
`IF`文は`ENDIF`で終了する。  
`SIF`文と違って必ず対応する`ENDIF`が必要なので注意。  

`GOTO TITLE_INPUT`は、ロードのときと違ってボタンが生きているので  
入力待機まで戻るだけのようだ。  

---  

## よくある誤りを読もう  
間違いやすいことを書いてくれている。詰まったときのために目を通してみよう。  
[よくある誤り](https://seesaawiki.jp/eraseries/d/%a4%e8%a4%af%a4%a2%a4%eb%b8%ed%a4%ea)

---  

## おわりに  
タイトル画面を追加しバニラにつなげるところまで作業した。  
eramaker側の処理の調べ方、最新化するための非推奨変数の調べ方、  
Emueraの命令や式中関数の調べ方などを案内できただろうか？  

書いている本人もいまだにわかっていないことがたくさんある。  
わからないことをどう調べたらいいのか、それを知ることが大事だと思う。  
慣れたつもりでも盲点は多い。いつまでも初心を失わないようにしたい。  

eraは調教シミュレーターなので、本来RPGやマップや戦略はない。  
ゲームを作るには、ツールの使い方だけではなく、  
レベルデザインや経路探索や思考ロジックなどを知る必要があり、eraはそれらを教えてはくれない。  
バニラやツールも含めて、すべて有志のかたがそれぞれに学んで作り出してくれた機能だ。称えたい。  

あなたが作りたいものを作る足掛かりになることを願う。  

---

Next page → [ERB creation practice](erawiki-ERBmanual.md)

<!--
----  
[[▲目次に戻る>#contents]]  
//  
//(2021/05/12 カスタムキャラの補足は直接更新修正してくださった方と更新部分がかぶり中途半端な状態になっています。  
//NOとの連携を忘れると大変なことになると教わり、カスタムキャラはまだ時期尚早かもしれないという話をして  
//一旦コメントアウトしました。修正改善、更新感謝。  
//FIND関連の最新化についてアドバイスくださったかたもありがとう。  
//  
//***カスタムキャラ絡みの補足  
//csv番号のないカスタムキャラを作り、削除できるようにしたシステムの場合  
//キャラを消すと登録されている番号も詰められる。CFLAGなども連動する。  
//これはカスタムキャラに限らず、CSVの存在するキャラを削除した場合でも番号は詰められる。  
//  
//これだとゲームを進めているうちに番号が動いてしまい、  
//童貞喪失の相手が誰だったか、などを保存しておけない。  
//  
//例として、  
//=||  
//;ここではCFLAG:0を「初めての相手のIDを記録するもの」とする  
//;TARGETは「調教対象」の登録番号である  
//CFLAG:登録番号:0 = TARGET  
//||=  
//上記の例文は、「登録番号」のキャラの、初めての相手をTARGETとして記録するものである。  
//キャラを削除した場合などはCFLAGの登録番号の部分は自動で処理されるため、ここでは気にしなくてもいい。  
//しかし、TARGETが1だったとして、1番目のキャラ名が「ファース子」、2番目のキャラが「セカン子」だった場合、  
//ファース子を削除されると番号は勝手に詰められ、セカン子が登録番号1番目のキャラになる  
//「CFLAG:登録番号:0」は1のまま変わらないため、初めての相手がセカン子に変わってしまう不具合が発生する。  
//また、「SORTCHARA」というキャラをソートする処理や、「SWAPCHARA」で登録番号を入れ替えた際にも同じ現象が起こる。  
//  
//そのため、キャラの参照は、  
//上の項で示したNO（CSV番号）などを使って、  
//CFLAG:登録番号:0 = NO:TARGET  
//などと保存することで不具合を防げる。  
//  
//ADDVOIDCHARAを使用したカスタムキャラの場合は全ての情報がまっさらな状態（0もしくは空文字）。NOも例外ではないため、  
//キャラ作成時に固有のNOを付けておく必要がある。  
//これが元々NOが0のことが多い「主人公」「あなた」なら問題は無いが、カスタムキャラが数人、もしくは全キャラがカスタムキャラだと、  
//多くの場面でNO:0が参照され、あらゆる場所に「あなた」が現れる地獄絵図と化す。  
//少し難しくはなるが、例として、  
//=||  
//ADDVOIDCHARA  
//NO:(CHARANUM-1) = FLAG:カスタムキャラ人数  
//FLAG:カスタムキャラ人数 += 1  
//||=  
//とすることで、固有のNOを割り当てることができる。  
//例文について詳しくは（今後作成されるであろう）次のページを参照。  
//  
//いざ相手のキャラを呼び出して使いたいときは、  
//=||  
//FOR ループカウンタ, 0, CHARANUM  
//	IF CFLAG:0 == NO:ループカウンタ  
//		呼びたいキャラ = ループカウンタ  
//		BREAK  
//	ENDIF  
//NEXT  
//||=  
//のように、ループ文で全キャラのNOをチェックし、一致した番号を検出して処理する  
//といった仕組みになる。  
//  
//これらのカスタムキャラの仕組みについてはこちらで詳しく説明してくれている。  
//|bgcolor(#F0F0E7):[[システム改造Q&A]]→実践編|  
//|https://seesaawiki.jp/eraseries/d/%a5%b7%a5%b9%a5%c6%a5%e0%b2%fe%c2%a4Q%26A#content_2|  
//----  
//  
//いざ相手のキャラを呼び出して使いたいときは、  
//式中で使える関数『FINDELEMENT(CFLAG:ID, キャラのID)』や『FINDCHARA()』を使うと良いようだ。  
//|bgcolor(#F0F0E7):EmueraWiki→eramaker basic 開発者向け情報→Emueraで追加された拡張文法→式中で使える関数→FINDELEMENT|  
//|https://ja.osdn.net/projects/emuera/wiki/exmeth#h5-int.20FINDELEMENT.20.28var.20array.2C.20.3F.20value.2C.20int.20start.20.3D.200.2C.20int.20end.20.3D.20.E2.80.BB.2C.20int.20flag.29|  
//|bgcolor(#F0F0E7):EmueraWiki→eramaker basic 開発者向け情報→Emueraで追加された拡張文法→式中で使える関数→FINDCHARA|  
//|https://ja.osdn.net/projects/emuera/wiki/exmeth#h5-int.20FINDCHARA.28var.20key.2C.20.3F.20value.2C.20int.20start.20.3D.200.2C.20int.20end.20.3D.20.E2.80.BB.29|  
//@ID_TO_CHARAの実装を、最新版のeraRanceKと  
//eratohoK ver1.29.3またはera恋姫とで比べてみても良いかもしれないとのこと。  
//----  
//[[▲目次に戻る>#contents]]  
//  
-->
