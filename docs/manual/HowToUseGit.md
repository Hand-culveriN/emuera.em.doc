# gitとはなにか

元となったページ
[eratohoまとめ V3 Gitの使い方](https://seesaawiki.jp/eratoho/d/Git%a4%ce%bb%c8%a4%a4%a4%ab%a4%bf)

---

<!--
//叩き台
//　たぶん言葉が難かったり分かりにくいところがあるからだれか修正求
-->
gitというのはフォルダとその中身の更新履歴を管理するためのツールでそのようなツールの中で現在最も洗練されたもののひとつです。
era界隈にとってはgitは

- バリアントのバージョンを記録する、パッチを統合することに対してそれらの作業の多くを自動化し作業量が減ること
- 開発において自身の作業内容を記録し、またそれらを基にパッチの差分ファイルの抽出を自動化すること

また、リモートリポジトリという、プロジェクトを公開しながら管理するgitにおいては、

- バリアント製作の進捗状況をプレイヤーが確認できる
- issueという形で要望や提案を一元管理できる
- 最新版のフルリリースが簡単にDL可能

<!--
//サーバーを用いる機能を解説に加えると説明に難解さが増えるので一旦排除　あとで後ろの方に発展的な内容で処理する
//- ローカルリポジトリでサーバを立てずに開発することが可能
//- 公開リポジトリで開発する場合、進行状況をユーザが知ることができる
-->

などに役立つことが期待されます。
一人では扱えないように書かれることもありますがgitは一人からでも利用できるようになっています
なにに使えるかの把握が難しいツールですがもし先にあげた役立つ状況に合致する場合はぜひ触れてみてください

**メリット**
- バリアントなどの過去バージョンをフォルダに分けしなくても確認、差し戻しができる
- パッチをまとめる作業がwinmergeよりも楽に
- 今までにやってきた作業内容を確認することができる

<!--
//変える必要が無いはデメリットが無いだけでメリットにはならない
//- 今までの開発方法を変える必要がなく導入できる
-->

**デメリット**
- gitを学習するためのコストが高い
- 過去のバージョンを管理する必要やパッチをまとめる必要が無いならほとんどメリットがない

---

## Q&A
分かりやすくするために質問を募集しています
<!--どこで質問を募っているか分からないので編集の拠点が定まったらリンクを貼る-->

## Gitクライアント
履歴を保存したり操作したりするためのソフト　どれでもだいたい似たようなことができる
いくつもありますがera界隈で使われているものを中心に紹介します

### [SourceTree](https://www.sourcetreeapp.com/)
GUIのgitクライアントの中でUIが優秀なものの一つ。
見やすくて操作も楽で公式日本語対応。
<!--今となってはもはや問題ではない
見やすくて操作も楽で公式日本語対応だけどVISTA以前のWindowsに対応していないという最大の問題点がある。
-->

### [TortoiseGit](https://tortoisegit.org/)
GUIのgitクライアント。
公式は英語だけど[[日本語化ファイル>https://osdn.jp/projects/tortoisegit/]]が別作者により配布されている、そしてVISTAでも使える。

### [Git for Windows](https://gitforwindows.org/)
もっとも公式にちかいWindows用git　コマンドラインがメインだがおまけでGUIがついてくる
もともとgitはこのサイトで配布する様なWinでいうコマンドプロンプトみたいな画面で操作するもののため
このgitクライアントを直接使うとよりgitの複雑な機能に触れられるかもしれない

<!--サーバーが死んでるのでコメントアウト
## era用ホストサーバ
https://emuera.git-server.com:8443/
era専用に建てられたサーバで、gitを使ってフォルダの履歴を共有することができます。
専用リポジトリを作るにはユーザ登録が必要です。
-->

---

## 入門サイト
### [サルでもわかるGit入門](https://backlog.com/ja/git-tutorial/) 難易度★
Backlogというサービスを運営している会社が自社サービスの売り込みのために作ったチュートリアル
ターゲットに全く知識がない相手を想定していたらしく内容は平易で絵やポイントが提示され読みやすい
ここで大雑把なところ覚えればいいが、入門中にBacklogという会社のサービスを使うように勧めてくるので注意
TortoiseGitを使っている

### [Gitの基礎勉強](http://tracpath.com/bootcamp/learning_git_firststep.html) 難易度★★
図入りでGitの仕組みの概要を説明している
コマンドラインを基にしているが図を使い平易な言葉で各用語の説明をする比較的分かりやすいものになっています

### [SourceTree の導入手順書](https://ux.getuploader.com/buppa3/download/183) 難易度★★★
eraコミュニティ用に書かれました
SourceTreeを導入してそれをどう操作すればいくつかのコマンドが操作できるかを指示する内容
他の入門でgitの用語を理解していることが前提とはなるがSourceTreeを導入する際の助けになるだろう

### [OSDNMagazine](https://osdn.jp/magazine/09/06/19/0340248) 難易度★★★
TortoiseGitのインストール手順
TortoiseGitをダウンロードして様々な操作を行う手順が書かれている
用語はほとんど説明がないためほかの入門で理解している必要がある

### [ドットインストール](https://dotinstall.com/lessons/basic_gitgithub) 難易度★★★★
動画で解説しているgitの入門
本家に近いコマンドライン形式のため内容は易しいが見た目で難しさを感じることだろう
git for windowsを使ってみたい人はこれを基に勉強してみよう

### [LearnGitBranching](http://k.swd.cc/learnGitBranching-ja/) 難易度★★★★
Gitのコマンドを模擬体験できるサイト
コマンドライン形式のgitを模した画面でパズルを解くように操作する感覚を養うことができます
右下にメニューが有るので何かあったらそこに頼ろう
<!--
//ここ昔使ったことあるんだけど謎なところで躓くよねこれ　大文字小文字とかコミットする順番とか
-->

### [Gitチュートリアル](https://www.atlassian.com/ja/git/) 難易度★★★★
Atlassian(SourceTreeを作ってる会社)のGit入門　コマンドライン形式
ある別のバージョン管理ソフトというものを触ったことがある人向けにチートシートを提供する内容
チュートリアルはまったくの初心者に向いているとは言えないが内容が簡潔できれいにまとまっている
また、このサイトは他と違い具体的なgitのブランチ、サーバーを活用したgitの運用アイデアの提供もされている
gitに慣れてきたら一度読んでみよう

### [Gitによるバージョン管理入門](http://www.plowman.co.jp/school/Git/Git.html) 難易度★★★★★
TortoiseGit を使った入門
<!--文体にちょっといらっと来る気がするけど-->
内容も文章量が多い代わり平易で内容も豊富
バージョン管理とは何ぞやを学ぶところから複雑なコマンドまで全部学びたい場合おすすめ

---

## その他参考資料
### [Pro git](https://git-scm.com/book/ja/v2)
gitを作ってる人たちが作ったgitの解説書
内容的にはこれからgitを始める人向けの本ではある
だが文章量が多くまたある程度類似ツールに熟練した人向けに書かれているためある程度慣れた人が読んでみるといいもの

## era版用語集
<!--
//eraの何かをいかすと入門サイトより分かりやすく説明できるかもしれないから一応用語集を作ってみた
-->
説明する用語募集中

### クライアント
サーバーと通信するためのツールという意味
gitはもともとフォルダの共有機能の拡張の結果ローカルだけでもフォルダの履歴管理ができるようになった経緯から

### リポジトリ
簡単に言うとセーブデータ貯蔵庫
セーブしたいフォルダごとに作られてそのフォルダの履歴がセーブされる

### ワーキングツリー、作業ツリー
セーブする対象になっているフォルダとその中のファイルのこと
実際に作業するディレクトリのことをさす

### INDEX,INDEXツリー
ワーキングツリーの内容からセーブする内容を取り出したもの
セーブするときはディレクトリで実際に作業した内容からどれをセーブするかをここに追加することで調整することができる
おそらくgitを学ぶ上で一番わかりにくいところだが、前のセーブデータから今現在どれだけ変化したのかを追跡するところだと思ってほしい

### コミット
セーブするということ
INDEXの中に入っているファイルをセーブすることができる
